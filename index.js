const express = require('express');

const app = express();

//Middleware
app.use(express.json())

// Routes
app.post('/new_user', (req,res) => {
    res.send(req.body)
})

app.post('/new_recipe', (req,res) => {
    res.send(req.body)
})

app.post('/rate', (req,res) => {
    res.send(req.body)
})

app.get('/recipes', (req,res) => {
    res.send("Enviamos las recetas")
})

app.get('/', (req,res) => {
    res.send("Bienvenido")
})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))